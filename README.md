# Kumpulan Tautan ISO distro Linux.

## Debian

- [Debian](https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd/debian-11.6.0-amd64-DVD-1.iso)
- [Debian netinstall](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.6.0-amd64-netinst.iso)
- [Debian non-free netinstall](https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/amd64/iso-cd/firmware-11.6.0-amd64-netinst.iso)

## Ubuntu dan Flavour-nya

- [Ubuntu](https://releases.ubuntu.com/jammy/ubuntu-22.04.2-desktop-amd64.iso)
- [Kubuntu](https://cdimage.ubuntu.com/kubuntu/releases/22.04.2/release/kubuntu-22.04.2-desktop-amd64.iso)
- [Lubuntu](https://cdimage.ubuntu.com/lubuntu/releases/22.04.2/release/lubuntu-22.04.2-desktop-amd64.iso)
- [Xubuntu](https://cdimage.ubuntu.com/xubuntu/releases/22.04.2/release/xubuntu-22.04.2-desktop-amd64.iso)
- [Ubuntu Mate](https://cdimage.ubuntu.com/ubuntu-mate/releases/22.04.2/release/ubuntu-mate-22.04.2-desktop-amd64.iso)
- [Ubuntu Budgie](https://cdimage.ubuntu.com/ubuntu-budgie/releases/22.04.2/release/ubuntu-budgie-22.04.2-desktop-amd64.iso)
- [Ubuntu DDE](https://bit.ly/ubuntudde-22-04-fosshost)
- [Ubuntu Unity](https://sourceforge.net/projects/ubuntu-unity/files/22.04.1/ubuntu-unity-22.04.1.iso/download)

## Turunan Ubuntu

- [Elementary OS](https://sgp1.dl.elementary.io/download/MTY3NzI0NjQ2NA==/elementaryos-7.0-stable.20230129rc.iso)
- [Linux Mint Cinnamon](https://mirrors.layeronline.com/linuxmint/stable/21.1/linuxmint-21.1-cinnamon-64bit.iso)
- [Linux Mint XFCE](https://mirrors.layeronline.com/linuxmint/stable/21.1/linuxmint-21.1-xfce-64bit.iso)
- [Linux Mint Mate](https://mirrors.layeronline.com/linuxmint/stable/21.1/linuxmint-21.1-mate-64bit.iso)
- [Linux Lite](https://osdn.net/dl/linuxlite/linux-lite-6.2-64bit.iso)
- [LXLE (64 bit)](https://sourceforge.net/projects/lxle/files/Final/OS/Focal-64/LXLE-Focal-Release.iso/download)
- [LXLE (32 bit)](https://sourceforge.net/projects/lxle/files/Final/OS/18.04.3-32/lxle-18043-32.iso/download)
- [Pop! OS ](https://iso.pop-os.org/22.04/amd64/intel/14/pop-os_22.04_amd64_intel_14.iso)
- [Pop! OS NVIDIA](https://iso.pop-os.org/22.04/amd64/nvidia/14/pop-os_22.04_amd64_nvidia_14.iso)
- [TUXEDO OS](https://os.tuxedocomputers.com/TUXEDO-OS-2-202302231702.iso)
- [Zorin OS Core](https://zorin.com/os/download/16/core)
- [Zorin OS Lite](https://zorin.com/os/download/16/lite)

## Archlinux

- [Archlinux](https://geo.mirror.pkgbuild.com/iso)

## Artix Linux

- [Artix Linux XFCE OpenRC](https://iso.artixlinux.org/iso/artix-xfce-openrc-20230215-x86_64.iso)
- [Artix Linux LXQT OpenRC](https://iso.artixlinux.org/iso/artix-lxqt-openrc-20230215-x86_64.iso)
- [Artix Linux KDE Plasma OpenRC](https://iso.artixlinux.org/iso/artix-plasma-openrc-20230215-x86_64.iso)

## Fedora

- [Fedora 36](https://download.fedoraproject.org/pub/fedora/linux/releases/36/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-36-1.5.iso)
- [Fedora 36 server standard](https://download.fedoraproject.org/pub/fedora/linux/releases/36/Server/x86_64/iso/Fedora-Server-dvd-x86_64-36-1.5.iso)
- [Fedora 36 server netinstall](https://download.fedoraproject.org/pub/fedora/linux/releases/36/Server/x86_64/iso/Fedora-Server-netinst-x86_64-36-1.5.iso)
- [Fedora 37](https://mirrors.tuna.tsinghua.edu.cn/fedora/releases/37/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-37-1.7.iso)
- [Fedora 37 server standard](https://mirrors.tuna.tsinghua.edu.cn/fedora/releases/37/Server/x86_64/iso/Fedora-Server-dvd-x86_64-37-1.7.iso)
- [Fedora 37 server netinstall](https://mirrors.tuna.tsinghua.edu.cn/fedora/releases/37/Server/x86_64/iso/Fedora-Server-netinst-x86_64-37-1.7.iso)
- [Fedora Silverblue](https://download.fedoraproject.org/pub/fedora/linux/releases/37/Silverblue/x86_64/iso/Fedora-Silverblue-ostree-x86_64-37-1.7.iso)
- [Fedora Kinoite](https://download.fedoraproject.org/pub/fedora/linux/releases/37/Kinoite/x86_64/iso/Fedora-Kinoite-ostree-x86_64-37-1.7.iso)

## Void

- [Void Linux Base](https://repo-default.voidlinux.org/live/current/void-live-x86_64-20221001-base.iso)
- [Void Live Xfce](https://repo-default.voidlinux.org/live/current/void-live-x86_64-20221001-xfce.iso)

## Alpine

- [Alpine Linux 3.16](https://dl-cdn.alpinelinux.org/alpine/v3.16/releases/x86_64/alpine-standard-3.16.2-x86_64.iso)
- [Alpine Linux 3.17](https://dl-cdn.alpinelinux.org/alpine/v3.17/releases/x86_64/alpine-standard-3.17.1-x86_64.iso)

## Opensuse

- [Opensuse Leap 15.4](https://download.opensuse.org/distribution/leap/15.4/iso/openSUSE-Leap-15.4-DVD-x86_64-Media.iso)
- [Opensuse Leap 15.5](https://download.opensuse.org/distribution/leap/15.5/iso/openSUSE-Leap-15.5-DVD-x86_64-Media.iso)
- [Opensuse Tumbleweed](https://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-DVD-x86_64-Current.iso)
- [openSUSE MicroOS](https://download.opensuse.org/tumbleweed/iso/openSUSE-MicroOS-DVD-x86_64-Current.iso)

## Endeavouros

- [Endeavouros](https://github.com/endeavouros-team/ISO/releases/download/1-EndeavourOS-ISO-releases-archive/EndeavourOS_Cassini_Nova-03-2023.iso)
- [Alternatif](https://mirror.freedif.org/EndeavourOS/iso/EndeavourOS_Cassini_Nova-03-2023.iso)
